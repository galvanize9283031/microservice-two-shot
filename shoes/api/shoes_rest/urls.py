from django.urls import path
from .views import api_shoes_list


urlpatterns = [
    # list all shoes
    path('shoes/', api_shoes_list, name='api_shoes_list'),
    # create a shoe
    path("bins/<int:id>/shoes/", api_shoes_list, name="api_shoes_list"),
    # remove a shoe
    path("shoes/<int:id>/", api_shoes_list, name="api_shoes_list"),
]
