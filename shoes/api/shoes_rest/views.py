from django.shortcuts import render
from common.json import ModelEncoder
from shoes_rest.models import BinVO, Shoes
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json


# Create your views here.


class BinVOEncoder(ModelEncoder):
    """
    This is a custom encoder for the BinVO model.
    """
    model = BinVO
    properties = ["closet_name", "import_href", "bin_number"]


class ShoesDetailEncoder(ModelEncoder):
    """
    This is a custom encoder for the Shoes model.
    """
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
        "id"
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST", "DELETE"])
def api_shoes_list(request, id=None):
    """
        GET: list all shoes - http://localhost:8080/api/shoes
        POST: create a shoe - http://localhost:8080/api/bins/1/shoes/
        And send a body like this:
        {
            "manufacturer": "Nike",
            "model_name": "Cloud",
            "color": "Blue",
            "picture_url": ""
        }
        DELETE: remove a shoe - http://localhost:8080/api/shoes/
        And send a body like this:
        {
            "id": 1
        }
    """
    if request.method == "GET":
        if id is not None:
            shoes = Shoes.objects.filter(id=id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesDetailEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            bin_href = f'/api/bins/{id}/'
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        content = json.loads(request.body)
        try:
            id = content["id"]
            shoe = Shoes.objects.get(id=id)
        except Shoes.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid shoe id"},
                status=400,
            )
        shoe.delete()
        return JsonResponse(
            {"message": "Shoe deleted"},
            status=200,
        )
