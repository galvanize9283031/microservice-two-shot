import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()


from shoes_rest.models import BinVO


def get_bin():
    """
    This function gets the bins from the wardrobe-api and updates the
    BinVO table.
    """
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    content = json.loads(response.content)
    for bin in content["bins"]:
        # print("in get_bin:", bin)
        BinVO.objects.update_or_create(
            import_href=bin["href"],
            defaults={
                "closet_name": bin["closet_name"],
                "bin_number": bin["bin_number"],
            },
        )


def poll():
    """
    This is the main polling loop.
    """
    while True:
        try:
            get_bin()
        except Exception as e:
            print(e)
        time.sleep(15)


if __name__ == "__main__":
    """
    This is the main entry point for the poller.
    """
    poll()
