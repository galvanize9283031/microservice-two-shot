from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import LocationVO, Hat

# Create your views here.
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
        "section_number"
    ]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id"
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }

@require_http_methods(["GET", "POST", "DELETE"])
def api_list_hats(request, id=None):
    if request.method == "GET":
        if id is not None:
            hats = Hat.objects.filter(id=id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            { "hats": hats},
            encoder=HatDetailEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            location_href = f'/api/locations/{id}/'
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        content = json.loads(request.body)
        try:
            id = content["id"]
            hat = Hat.objects.get(id=id)
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid hat id"},
                status = 400,
            )
        hat.delete()
        return JsonResponse(
            {"message": "Hat deleted"},
            status=200,
        )
