from django.urls import path
from .api_views import api_list_hats


urlpatterns = [
    path('hats/', api_list_hats, name="api_list_hats"),
    path('hats/<int:id>/', api_list_hats, name="api_list_hats"),
    path('locations/<int:id>/hats/', api_list_hats, name="api_list_hats"),
]
