import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
# from hats_rest.models import Something
from hats_rest.models import LocationVO

def get_location():
    """
    gets the locations from wardrobe-api and updates the LocationVO table
    """
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)
    for location in content["locations"]:
        # print("in get_location:", location)
        LocationVO.objects.update_or_create(
            import_href=location["href"],
            defaults={
                "closet_name": location["closet_name"],
                "section_number": location["section_number"],
            },
        )

def poll():
    while True:
        # print('Hats poller polling for data')
        try:
            # Write your polling logic, here
            get_location()
        except Exception as e:
            print(e)
        time.sleep(15)


if __name__ == "__main__":
    """
    This is the main entry point for the poller.
    """
    poll()
