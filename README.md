# Wardrobify

## Team
- Howard - Hat microservice.
- Marcio - Shoes microservice.

## Installation
To install and run the system, download the docker image and:
- Run "docker-compose build"
- Run "docker-compose up"
- Point your browser to the URL: http://localhost:3000/

## Design
The figure below illustrates the system, which consists of seven subsystems, each with its own unique purpose. These subsystems are responsible for managing and storing data, and providing API access to the data.

The subsystems are as follows:

- Wardrobe and associated database: This subsystem provides the data model for the bins and locations, stores its data on the database, and offers an API to access the data.
- Shoes Microservice and associated database: This subsystem stores information about the shoes and provides an API that allows the front-end application to access its data.
- Hats Microservice and associated database: This subsystem stores information about the hats and provides an API that allows the front-end application to access its data.
- The front-end application running in the browser: This subsystem provides the graphical user interface (GUI) through which users can interact with the system.
- Three instances of a database to store the data of their associated services: These subsystems store the data associated with their respective services.

All these services are packaged in a single docker image, making it easy to deploy and access. Once deployed, access the system by pointing your browser to the URL: http://localhost:3000/.

![alt text](overview.png "System's overview")
## Shoes microservice
### The models implemented are:
- Manufacturer
- ModelName
- Color
- UrlPic
- Bin (foreign key)

### The shoe service provides the following APIs:
- GET: list all shoes - http://localhost:8080/api/shoes
- POST: create a shoe - http://localhost:8080/api/bins/BIN_ID/shoes/ and this JSON model in the body:
```
{
  "manufacturer": "Nike",
  "model_name": "Cloud",
  "color": "Blue",
  "picture_url": "URL to the image"
}
```
- DELETE: remove a shoe - http://localhost:8080/api/shoes/ and this JSON model in the body:
```
{
  "id": 1
}
```

## Hats microservice

### The models implemented are:
- Fabric
- StyleName
- Color
- UrlPic
- Location (foreign key)

### The hats services provides the following APIs:
- GET: list all hats - http://localhost:8090/api/hats
- POST: create a hat - http://localhost:8090/api/locations/LOCATION_ID/hats/
and this JSON model in the body:
```
{
  "fabric": "silk",
  "style_name": "glossy",
  "color": "pink",
  "picture_url": "URL to the image"
}
```
- DELETE: remove a hat - http://localhost:8090/api/hats/ and this JSON model in the body:
```
{
  "id": 1
}
```
