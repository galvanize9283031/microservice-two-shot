import React, { useState, useEffect } from "react";
import "./ListShoes.css"

function ListShoes() {
    const [shoes, setShoes] = useState([]);

    const getListOfShoes = async () => {
        const response = await fetch("http://localhost:8080/api/shoes/");
        if (response.status === 200) {
            setShoes((await response.json()).shoes);
        }
    }

    useEffect(() => {
        getListOfShoes();
    }, []);

    const handleDeleteClick = async (event) => {
        event.preventDefault();
        const shoeId = event.target.dataset.key;
        const response = await fetch(`http://localhost:8080/api/shoes/`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                id: shoeId,
            })
        });
        if (response.status === 200) {
            console.log("Shoe deleted!");
            getListOfShoes();
        }
    }

    const createCard = (shoe) => {
        return (
            <div className="card shadow p-4 mt-4" key={shoe.id}>
                <div className="d-flex flex-column card-body">
                    <h5 className="card-title">{shoe.manufacturer}</h5>
                    <h6 className="card-subtitle mb-2 text-muted">{shoe.model_name}</h6>
                    <h6 className="card-subtitle mb-2 text-muted">{shoe.color}</h6>
                    <h6 className="card-subtitle mb-2 text-muted">Bin: {shoe.bin.closet_name}, {shoe.bin.bin_number}</h6>
                    <div className="my-3">
                        <img style={{ maxWidth: "100%" }} src={shoe.picture_url} alt={shoe.model_name} />
                    </div>
                    <div className="btn-container">
                        <button onClick={handleDeleteClick} className="btn btn-secondary btn-sm mt-2" data-key={shoe.id}>Delete</button>
                    </div>
                </div>
            </div>
        )
    }

    return (
        <div className="d-flex px-4 flex-column align-items-center">
            <section className="shoes-container">
                {shoes.map((shoe) => createCard(shoe))}
            </section>
        </div>
    )
}

export default ListShoes;
