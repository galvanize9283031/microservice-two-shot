import React, { useState } from "react";
import ListShoes from "./ListShoes";
import CreateShoes from "./CreateShoes";
import CreateBins from "./CreateBins";

function ShoesPage() {
    const [showList, setShowlist] = useState(0);

    return (
        <>
            <h1 className="text-center my-2">Shoes</h1>
            <div className="d-flex flex-row">
                <div className="d-flex flex-column col-2 mt-5">
                    <button onClick={() => setShowlist(0)} className="btn btn-primary my-1 mx-1">List Shoes</button>
                    <button onClick={() => setShowlist(1)} className="btn btn-primary my-1 mx-1">Add Shoe</button>
                    <button onClick={() => setShowlist(2)} className="btn btn-primary my-1 mx-1">Create Bin</button>
                </div>
                <div className="d-flex col-10 justify-content-center" id="contentArea">
                    { [<ListShoes />, <CreateShoes />, <CreateBins />][showList] }
                </div>
            </div>
        </>
    );
}

export default ShoesPage;
