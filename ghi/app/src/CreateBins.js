import React from "react";

/*
Create a new bin by doing:
POST: http://localhost:8100/api/bins/
And send a body like this:
{
	"closet_name": "my_closet",
	"bin_number": 1,
	"bin_size": 100
}
*/

function CreateBins() {
  // Closet name
  const [closetName, setClosetName] = React.useState("");
  const handleClosetNameChange = (event) => {
    setClosetName(event.target.value);
  };
  // Bin number
  const [binNumber, setBinNumber] = React.useState("");
  const handleBinNumberChange = (event) => {
    setBinNumber(event.target.value);
  };
  // Bin size
  const [binSize, setBinSize] = React.useState("");
  const handleBinSizeChange = (event) => {
    setBinSize(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch("http://localhost:8100/api/bins/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        closet_name: closetName,
        bin_number: binNumber,
        bin_size: binSize,
      }),
    });
    if (response.status === 200) {
      setBinNumber("");
      setBinSize("");
      setClosetName("");
      document.getElementById("create_bin_form").reset();
      console.log("Bin created!");
    } else {
      console.log("Bin creation failed!");
    }
  };

  return (
    <div className="row">
      <div className="">
        <div className="shadow p-4 mt-4">
          <h3>Add a new bin</h3>
          <form onSubmit={handleSubmit} id="create_bin_form">
            <div className="form-floating mb-3">
              <input
                onChange={handleClosetNameChange}
                placeholder="Closet name"
                required
                type="text"
                name="closet_name"
                id="closet_name"
                className="form-control"
              />
              <label htmlFor="manufacturer">Closet name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleBinNumberChange}
                placeholder="Bin number"
                required
                type="number"
                name="bin_number"
                id="bin_number"
                className="form-control"
              />
              <label htmlFor="bin_number">Bin number</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleBinSizeChange}
                placeholder="Bin size"
                required
                type="number"
                name="bin_size"
                id="bin_size"
                className="form-control"
              />
              <label htmlFor="bin_size">Bin size</label>
            </div>
            <button className="btn btn-primary">Add</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateBins;
