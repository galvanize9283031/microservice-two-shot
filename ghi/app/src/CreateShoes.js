import React from "react";

/*
Create a new shoe by doing:
POST: create a shoe - http://localhost:8080/api/bins/{bin_id}/shoes/
And send a body like this:
{
    "manufacturer": "Nike",
    "model_name": "Cloud",
    "color": "Blue",
    "picture_url": ""
}
*/

const defaultLink = "https://png.pngtree.com/back_origin_pic/04/63/68/bd6de885b7268afbe7d1018ebb3c46ff.jpg"

function CreateShoes() {
    // Bins
    // fetch all bins with http://localhost:8100/api/bins/
    const [bins, setBins] = React.useState([]);
    const fetchBins = async () => {
        const response = await fetch("http://localhost:8100/api/bins/");
        const data = await response.json();
        setBins(data.bins);
    }
    React.useEffect(() => {
        fetchBins();
    }, []);

    // Manufacturer
    const [manufacturer, setManufacturer] = React.useState("");
    const handleManufacturerChange = (event) => {
        setManufacturer(event.target.value);
    }
    // Model name
    const [modelName, setModelName] = React.useState("");
    const handleModelNameChange = (event) => {
        setModelName(event.target.value);
    }
    // Color
    const [color, setColor] = React.useState("");
    const handleColorChange = (event) => {
        setColor(event.target.value);
    }
    // Picture URL
    const [pictureUrl, setPictureUrl] = React.useState("");
    const handlePictureUrlChange = (event) => {
        setPictureUrl(event.target.value);
    }
    // Bin
    const [bin, setBin] = React.useState("");
    const handleBinChange = (event) => {
        setBin(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        let url = pictureUrl;
        if (url === "") url = defaultLink;
        const response = await fetch(`http://localhost:8080/api/bins/${bin}/shoes/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                manufacturer: manufacturer,
                model_name: modelName,
                color: color,
                picture_url: url
            })
        });
        if (response.status === 200) {
            console.log("Shoe created!");
            setManufacturer("");
            setModelName("");
            setColor("");
            setPictureUrl("");
            setBin("");
            document.getElementById("create_shoe_form").reset();
        } else {
            console.log("Error creating shoe!");
        }
    }

    return (
        <div className="row">
            <div className="">
                <div className="shadow p-4 mt-4">
                    <h3>Add a new shoe</h3>
                    <form onSubmit={handleSubmit} id="create_shoe_form">
                        <div className="form-floating mb-3">
                            <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleModelNameChange} placeholder="Model name" required type="text" name="model_name" id="model_name" className="form-control" />
                            <label htmlFor="model_name">Model name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlChange} placeholder="Picture URL" type="url" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleBinChange} required id="bin" className="form-select">
                                <option value="">Select a bin</option>
                                {
                                    bins.sort().map(bin => {
                                        return (
                                            <option key={bin.id} value={bin.id} text={bin.closet_name + ": " + bin.bin_number}>
                                                {bin.closet_name + ": " + bin.bin_number}
                                            </option>
                                        );
                                    })
                                }
                            </select>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CreateShoes;
