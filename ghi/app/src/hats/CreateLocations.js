import React from "react";

/*
Create a new location by doing:
POST: http://localhost:8100/api/locations/
And send a body like this:
{
	"closet_name": "Wizard of Oz",
	"section_number": 1,
	"shelf_number": 1
}
*/

function CreateLocations() {
  // Closet name
  const [closetName, setClosetName] = React.useState("");
  const handleClosetNameChange = (event) => {
    setClosetName(event.target.value);
  };
  // Location section number
  const [sectionNumber, setSectionNumber] = React.useState("");
  const handleLocationSectionNumberChange = (event) => {
    setSectionNumber(event.target.value);
  };
  // Location shelf number
  const [shelfNumber, setShelfNumber] = React.useState("");
  const handleShelfNumberChange = (event) => {
    setShelfNumber(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch("http://localhost:8100/api/locations/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        closet_name: closetName,
        section_number: sectionNumber,
        shelf_number: shelfNumber,
      }),
    });
    if (response.status === 200) {
      setClosetName("");
      setSectionNumber("");
      setShelfNumber("");
      // what element is this?
      document.getElementById("create_location_form").reset();
      console.log("Location created!");
    } else {
      console.log("Location creation failed!");
    }
  };

  return (
    <div className="row">
      <div className="">
        <div className="shadow p-4 mt-4">
          <h3>Add a new location</h3>
          <form onSubmit={handleSubmit} id="create_location_form">
            <div className="form-floating mb-3">
              <input
                onChange={handleClosetNameChange}
                placeholder="Closet name"
                required
                type="text"
                name="closet_name"
                id="closet_name"
                className="form-control"
              />
              <label htmlFor="manufacturer">Closet name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleLocationSectionNumberChange}
                placeholder="Section number"
                required
                type="number"
                name="section_number"
                id="section_number"
                className="form-control"
              />
              <label htmlFor="section_number">Section number</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleShelfNumberChange}
                placeholder="Shelf number"
                required
                type="number"
                name="shelf_number"
                id="shelf_number"
                className="form-control"
              />
              <label htmlFor="shelf_number">Shelf number</label>
            </div>
            <button className="btn btn-primary">Add</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateLocations;
