import React, { useState, useEffect } from "react";
import "./ListHats.css";

function ListHats() {
  const [hats, setHats] = useState([]);

  const getListOfHats = async () => {
    const response = await fetch("http://localhost:8090/api/hats");
    if (response.status === 200) {
      setHats((await response.json()).hats);
    }
  };

  useEffect(() => {
    getListOfHats();
  }, []);

  const handleDeleteClick = async (event) => {
    event.preventDefault();
    const HatID = event.target.dataset.key;
    const response = await fetch(`http://localhost:8090/api/hats`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: HatID,
      }),
    });
    if (response.status === 200) {
      console.log("Hat deleted!");
      getListOfHats();
    }
  };

  const createCard = (hat) => {
    return (
      <div className="card shadow p-4 mt-4" key={hat.id}>
        <div className="d-flex flex-column card-body">
          <h5 className="card-title">{hat.style_name}</h5>
          <h6 className="card-subtitle mb-2 text-muted">{hat.fabric}</h6>
          <h6 className="card-subtitle mb-2 text-muted">{hat.color}</h6>
          <h6 className="card-subtitle mb-2 text-muted">
            Location: {hat.location.closet_name},{hat.location.section_number}
          </h6>
          <div className="my-3">
            <img
              style={{ maxWidth: "100%" }}
              src={hat.picture_url}
              alt={hat.style_name}
            />
          </div>
          <div className="btn-container">
            <button
              onClick={handleDeleteClick}
              className="btn btn-secondary btn-sm mt-2"
              data-key={hat.id}
            >
              Delete
            </button>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="d-flex px-4 flex-column align-items-center">
      <section className="hats-container">
        {hats.map((hat) => createCard(hat))}
      </section>
    </div>
  );
}

export default ListHats;
