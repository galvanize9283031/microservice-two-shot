import React from "react";

/*
Create a new hat by doing:
POST: create a hat - http://localhost:8090/api/locations/{location_id}/hats/
And send a body like this:
{
    "fabric": "straw",
    "style_name": "fedora",
    "color": "tan",
    "picture_url": "URL to the image"
}
*/

const defaultLink =
  "https://hatworld.com.au/cdn/shop/products/20190306_104057.jpg?v=1690862992&width=823";

function CreateHats() {
  // Locations
  // fetch all locations with http://localhost:8100/api/locations/
  const [locations, setLocations] = React.useState([]);
  const fetchLocations = async () => {
    const response = await fetch("http://localhost:8100/api/locations/");
    const data = await response.json();
    setLocations(data.locations);
    console.log("locations:", data.locations);
  };
  React.useEffect(() => {
    fetchLocations();
  }, []);

  // Fabric
  const [fabric, setFabric] = React.useState("");
  const handleFabricChange = (event) => {
    setFabric(event.target.value);
  };
  // Style name
  const [styleName, setStyleName] = React.useState("");
  const handleStyleNameChange = (event) => {
    setStyleName(event.target.value);
  };
  // Color
  const [color, setColor] = React.useState("");
  const handleColorChange = (event) => {
    setColor(event.target.value);
  };
  // Picture URL
  const [pictureUrl, setPictureUrl] = React.useState("");
  const handlePictureUrlChange = (event) => {
    setPictureUrl(event.target.value);
  };
  // Location
  const [location, setLocation] = React.useState("");
  const handleLocationChange = (event) => {
    setLocation(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    let url = pictureUrl;
    // should display placeholder hat image
    if (url === "") url = defaultLink;
    const response = await fetch(
      `http://localhost:8090/api/locations/${location}/hats/`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          fabric: fabric,
          style_name: styleName,
          color: color,
          picture_url: url,
        }),
      }
    );
    if (response.status === 200) {
      console.log("Hat created!");
      setFabric("");
      setStyleName("");
      setColor("");
      setPictureUrl("");
      setLocation("");
      document.getElementById("create-hat-form").reset();
    } else {
      console.log("Error creating hat!");
    }
  };

  return (
    <div className="row">
      <div className="">
        <div className="shadow p-4 mt-4">
          <h3>Add a new hat</h3>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFabricChange}
                placeholder="Fabric"
                required
                type="text"
                name="fabric"
                id="fabric"
                className="form-control"
              />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleStyleNameChange}
                placeholder="Style name"
                required
                type="text"
                name="style_name"
                id="style_name"
                className="form-control"
              />
              <label htmlFor="style_name">Style name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleColorChange}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handlePictureUrlChange}
                placeholder="Picture URL"
                type="url"
                name="picture_url"
                id="picture_url"
                className="form-control"
              />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleLocationChange}
                required
                id="location"
                className="form-select"
              >
                <option value="">Select a location</option>
                {locations.sort().map((location) => {
                  return (
                    <option
                      key={location.id}
                      value={location.id}
                      text={
                        location.closet_name + ": " + location.section_number
                      }
                    >
                      {location.closet_name + ": " + location.section_number}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Add</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateHats;
