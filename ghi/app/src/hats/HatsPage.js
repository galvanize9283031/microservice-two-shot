import React, { useState } from "react";
import ListHats from "./ListHats";
import CreateHats from "./CreateHats";
import CreateLocations from "./CreateLocations";

function HatsPage() {
  const [showList, setShowlist] = useState(0);

  return (
    <>
      <h1 className="text-center my-2">Hats</h1>
      <div className="d-flex flex-row">
        <div className="d-flex flex-column col-2 mt-5">
          <button
            onClick={() => setShowlist(0)}
            className="btn btn-primary my-1 mx-1"
          >
            List Hats
          </button>
          <button
            onClick={() => setShowlist(1)}
            className="btn btn-primary my-1 mx-1"
          >
            Add Hat
          </button>
          <button
            onClick={() => setShowlist(2)}
            className="btn btn-primary my-1 mx-1"
          >
            Create Location
          </button>
        </div>
        <div className="d-flex col-10 justify-content-center" id="contentArea">
          {[<ListHats />, <CreateHats />, <CreateLocations />][showList]}
          {/* {[<CreateHats />, <CreateLocations />][showList]} */}
          {/* {[<CreateLocations />][showList]} */}
        </div>
      </div>
    </>
  );
}

export default HatsPage;
