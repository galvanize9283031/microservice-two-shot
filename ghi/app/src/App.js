import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ShoesPage from "./ShoesPage";
import HatsPage from "./hats/HatsPage";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoesPage />} />
          <Route path="/hats" element={<HatsPage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
